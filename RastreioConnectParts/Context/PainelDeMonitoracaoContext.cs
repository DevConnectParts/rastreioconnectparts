﻿using RastreioConnectParts.Entity;
using System.Data.Entity;

namespace RastreioConnectParts.Context
{
    public class PainelDeMonitoracaoContext : DbContext
    {
        public PainelDeMonitoracaoContext() : base("DbConnectionString")
        {
            Database.SetInitializer<PainelDeMonitoracaoContext>(null);
        }

        public DbSet<RastreioTransportadora> RastreioTransportadoras { get; set; }
    }
}