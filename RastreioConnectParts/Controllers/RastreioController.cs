﻿using Atlassian.Jira.Remote;
using Newtonsoft.Json;
using RastreioConnectParts.Context;
using RastreioConnectParts.CorreiosRastro;
using RastreioConnectParts.Models;
using RastreioConnectParts.Models.Correios;
using RastreioConnectParts.Models.Intelipost;
using RastreioConnectParts.Models.Loggi;
using RastreioConnectParts.Models.NowLog;
using RastreioConnectParts.Models.Rodonaves;
using RastreioConnectParts.Models.Transfolha;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace RastreioConnectParts.Controllers
{
    public class RastreioController : Controller
    {
        private PainelDeMonitoracaoContext db = new PainelDeMonitoracaoContext();

        [HttpGet]
        public ActionResult Index(string c)
        {
            var transportadoras = db.RastreioTransportadoras.ToList();

            if (!String.IsNullOrEmpty(c))
            {
                PedidoRastreio pedido = RecuperaPedido(c);

                if (pedido == null)
                {
                    string msg = "Não foi possível localizar seu pedido.";
                    ViewData["msg"] = msg;
                    return View("Error");
                }

                string cnpj = CnpjRastreio(pedido.PedidoUnidadeNegocio);

                Rastreio rastreio = new Rastreio();

                #region Gol Log
                if (pedido.PedidoTransportadoraCodigo == 5 && transportadoras.Where(t => t.Nome == "Gol Log").FirstOrDefault().RastreioIntelipost == false)
                {
                    return Redirect($"https://edi.voegol.com.br/ediportal/rastreamento/padrao.aspx?cnpj={cnpj}&modo=0&notafiscal={pedido.NotaFiscalNumero}");
                }
                #endregion

                #region TNT
                if (pedido.PedidoTransportadoraCodigo == 4 && transportadoras.Where(t => t.Nome == "TNT").FirstOrDefault().RastreioIntelipost == false)
                {
                    return Redirect($"https://radar.tntbrasil.com.br/radar/public/eventoNotaFiscalCliente/{cnpj}/{pedido.NotaFiscalNumero}-{pedido.NotaFiscalSerie}");
                }
                #endregion

                try
                {
                    rastreio = Rastrear(pedido);
                }
                catch (Exception e)
                {
                    string msg = "Seu pedido está em processamento de transporte.";
                    ViewData["msg"] = msg;
                    return View("Error");
                }

                ViewBag.Movimentacao = rastreio.Movimentos;
                ViewBag.DataEstimada = rastreio.DataEstimada;
                ViewBag.Metodo = rastreio.Metodo;
                ViewBag.Transportadora = rastreio.Transportadora;
                ViewBag.Status = rastreio.Status;

                return View();
            }

            return RedirectToAction("Pesquisar");

        }

        [HttpPost]
        public ActionResult Pesquisar(string codigo)
        {
            return RedirectToAction("Index", new { c = codigo });
        }

        [HttpGet]
        public ActionResult Pesquisar()
        {

            return View();
        }

        /// <summary>
        /// Recupera o obj de pedido simplificado do Ábacos baseando-se apenas no número de rastreio do mesmo
        /// </summary>
        /// <param name="numeroRastreio">Número de rastreio</param>
        /// <returns>Objeto de pedido simplifacado</returns>
        public PedidoRastreio RecuperaPedido(string numeroRastreio)
        {
            RestClient client = new RestClient("http://api.dakotaparts.com.br/Fenix");
            string recurso = $"/Frete/BuscarSimples/?&numeroRastreio={numeroRastreio}";

            var requisicao = new RestRequest(recurso, Method.GET)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            requisicao.AddHeader("Accept", "application/json");
            requisicao.AddHeader("Content-Type", "application/json");

            IRestResponse resposta = client.Execute(requisicao);

            PedidoRastreio pedido_rastreio = JsonConvert.DeserializeObject<PedidoRastreio>(resposta.Content);

            return pedido_rastreio;
        }

        /// <summary>
        /// Define qual o CNPJ (unidade fiscal ou matriz) a ser utilizado para rastreio
        /// </summary>
        /// <param name="unidadeNegocio">Unidade de negócio do pedido</param>
        /// <returns>CPNJ de rastreio</returns>
        public string CnpjRastreio(int unidadeNegocio)
        {
            if (unidadeNegocio == 1)
            {
                return "08677036000116";
            }
            else if (unidadeNegocio == 5)
            {
                return "08677036000388";
            }
            return "";
        }

        /// <summary>
        /// Rastreia o pedido de acordo com o parametro de rastreio ativo no Painel do TI, podendo ser via transportadora ou INtelipost
        /// </summary>
        /// <param name="pedido">Objeto de pedido simplificado</param>
        /// <returns>Lista de movimentação dos pacotes</returns>
        public Rastreio Rastrear(PedidoRastreio pedido)
        {
            string cnpj = CnpjRastreio(pedido.PedidoUnidadeNegocio);
            List<Movimento> eventos = new List<Movimento>();
            Rastreio rastreio = new Rastreio();

            var transportadoras = db.RastreioTransportadoras.ToList();

            #region Loggi
            if (pedido.PedidoTransportadoraCodigo == 53)
            {
                if (transportadoras.Where(t => t.Nome == "Loggi").FirstOrDefault().RastreioIntelipost == false)
                {
                    rastreio.Movimentos = GerarEventosViaLoggi(pedido);
                }
                else
                {
                    rastreio = GerarEventosViaIntelipost(pedido);
                }

            }
            #endregion

            #region NowLog
            if (pedido.PedidoTransportadoraCodigo == 45)
            {
                if (transportadoras.Where(t => t.Nome == "NowLog").FirstOrDefault().RastreioIntelipost == false)
                {
                    rastreio.Movimentos = GerarEventosViaNowLog(pedido, cnpj);
                }
                else
                {
                    rastreio = GerarEventosViaIntelipost(pedido);
                }

            }
            #endregion

            #region Rodonaves
            if (pedido.PedidoTransportadoraCodigo == 8)
            {
                if (transportadoras.Where(t => t.Nome == "Rodonaves").FirstOrDefault().RastreioIntelipost == false)
                {
                    rastreio.Movimentos = GerarEventosViaRodonaves(pedido, cnpj);
                }
                else
                {
                    rastreio = GerarEventosViaIntelipost(pedido);
                }
            }
            #endregion

            #region Transfolha
            if (pedido.PedidoTransportadoraCodigo == 44)
            {
                if (transportadoras.Where(t => t.Nome == "Transfolha").FirstOrDefault().RastreioIntelipost == false)
                {
                    rastreio.Movimentos = GerarEventosViaTransfolha(pedido);
                }
                else
                {
                    rastreio = GerarEventosViaIntelipost(pedido);
                }
            }
            #endregion

            #region Correios
            if (pedido.PedidoTransportadoraCodigo == 1 || pedido.PedidoTransportadoraCodigo == 58 || pedido.PedidoTransportadoraCodigo == 61)
            {
                if (transportadoras.Where(t => t.Nome == "Correios").FirstOrDefault().RastreioIntelipost == false)
                {
                    rastreio.Movimentos = GerarEventosViaCorreios(pedido);
                }
                else
                {
                    rastreio = GerarEventosViaIntelipost(pedido);
                }
            }
            #endregion

            #region Gol Log
            if (pedido.PedidoTransportadoraCodigo == 5 && transportadoras.Where(t => t.Nome == "Gol Log").FirstOrDefault().RastreioIntelipost == true)
            {
                rastreio = GerarEventosViaIntelipost(pedido);
            }
            #endregion

            #region TNT
            if (pedido.PedidoTransportadoraCodigo == 4 && transportadoras.Where(t => t.Nome == "TNT").FirstOrDefault().RastreioIntelipost == true)
            {
                rastreio = GerarEventosViaIntelipost(pedido);
            }
            #endregion

            return rastreio;
        }

        /// <summary>
        /// Gera a listagem de eventos de rastreio Intelipost com tratamento direcionado para transpotadora no caso de ocorrência de erro
        /// </summary>
        /// <param name="pedido">Objeto pedido</param>
        /// <returns>Lista de enventos do objeto rastreado</returns>
        public Rastreio GerarEventosViaIntelipost(PedidoRastreio pedido)
        {
            List<Movimento> eventos = new List<Movimento>();
            Rastreio rastreio = new Rastreio();

            IntelipostRastreio rastreioIntelipost = RastrearIntelipost(pedido.NumeroRastreio);

            try
            {
                rastreio.DataEstimada = rastreioIntelipost.content[0].estimated_delivery_date_iso;
                rastreio.Transportadora = rastreioIntelipost.content[0].logistic_provider_name;
                rastreio.Metodo = rastreioIntelipost.content[0].delivery_method_name;
                rastreio.Status = rastreioIntelipost.content[0].shipment_order_volume_array[0].shipment_order_volume_state_localized;

                if (rastreioIntelipost.content[0].shipment_order_volume_array[0].shipment_order_volume_state_history_array.Any())
                {
                    foreach (var eventoIntelipost in rastreioIntelipost.content[0].shipment_order_volume_array[0].shipment_order_volume_state_history_array)
                    {
                        Movimento movimento = new Movimento();
                        movimento.Evento = eventoIntelipost.shipment_volume_micro_state.shipment_volume_state_localized;
                        movimento.Detalhes = eventoIntelipost.shipment_volume_micro_state.description;
                        movimento.Observacao = eventoIntelipost.provider_message;
                        movimento.Data = Convert.ToDateTime(eventoIntelipost.event_date_iso);
                        eventos.Add(movimento);
                    }
                }
            }
            catch
            {
                string cnpj = CnpjRastreio(pedido.PedidoUnidadeNegocio);

                if (pedido.PedidoTransportadoraCodigo == 53)
                {
                    eventos = GerarEventosViaLoggi(pedido);
                }

                if (pedido.PedidoTransportadoraCodigo == 45)
                {
                    eventos = GerarEventosViaNowLog(pedido, cnpj);
                }

                if (pedido.PedidoTransportadoraCodigo == 8)
                {
                    eventos = GerarEventosViaRodonaves(pedido, cnpj);
                }

                if (pedido.PedidoTransportadoraCodigo == 44)
                {
                    eventos = GerarEventosViaTransfolha(pedido);
                }

                if (pedido.PedidoTransportadoraCodigo == 1 || pedido.PedidoTransportadoraCodigo == 58 || pedido.PedidoTransportadoraCodigo == 61)
                {
                    eventos = GerarEventosViaCorreios(pedido);
                }
            }           

            rastreio.Movimentos = eventos;

            return rastreio;
        }

        /// <summary>
        /// Gera a listagem de eventos de rastreio Loggi
        /// </summary>
        /// <param name="pedido">Número de rastreio</param>
        /// <returns>Lista de enventos do objeto rastreado</returns>
        public List<Movimento> GerarEventosViaLoggi(PedidoRastreio pedido)
        {
            List<Movimento> eventos = new List<Movimento>();
            LoggiRastreio rastreioLoggi = RastreioLoggi(pedido.NumeroRastreio);
            foreach (var eventoLoggi in rastreioLoggi.history)
            {
                Movimento movimento = new Movimento();
                movimento.Evento = eventoLoggi.status.message;
                movimento.Data = ConverterData(eventoLoggi.status.updated_at);
                eventos.Add(movimento);
            }

            return eventos;
        }

        /// <summary>
        /// Gera a listagem de eventos de rastreio NowLog
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <param name="cnpj">CNPJ da unidade</param>
        /// <returns></returns>
        public List<Movimento> GerarEventosViaNowLog(PedidoRastreio pedido, string cnpj)
        {
            List<Movimento> eventos = new List<Movimento>();
            NowLogRastreio rastreioNowLog = RastreioNowLog(pedido, cnpj);
            foreach (var eventoNowLog in rastreioNowLog.tracking)
            {
                Movimento movimento = new Movimento();
                movimento.Detalhes = eventoNowLog.descricao;
                movimento.Evento = eventoNowLog.ocorrencia;
                movimento.Observacao = eventoNowLog.cidade;
                movimento.Data = Convert.ToDateTime(eventoNowLog.data_hora_efetiva);
                eventos.Add(movimento);
            }

            return eventos;
        }

        /// <summary>
        /// Gera a listagem de eventos de rastreio Rodonaves
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <param name="cnpj">CNPJ da unidade</param>
        /// <returns></returns>
        public List<Movimento> GerarEventosViaRodonaves(PedidoRastreio pedido, string cnpj)
        {
            List<Movimento> eventos = new List<Movimento>();
            RodonavesRastreio rastreioRodonaves = RastreioRodonaves(pedido, cnpj);
            foreach (var eventoRodonaves in rastreioRodonaves.Events)
            {
                Movimento movimento = new Movimento();
                movimento.Evento = eventoRodonaves.Description;
                movimento.Data = Convert.ToDateTime(eventoRodonaves.Date);
                eventos.Add(movimento);
            }

            return eventos;
        }

        /// <summary>
        /// Gera a listagem de eventos de rastreio Transfolha
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <returns></returns>
        public List<Movimento> GerarEventosViaTransfolha(PedidoRastreio pedido)
        {
            List<Movimento> eventos = new List<Movimento>();
            TransfolhaRastreio rastreioTransfolha = RastreioTransfolha(pedido);
            foreach (var eventoTransfolha in rastreioTransfolha.Table1)
            {
                Movimento movimento = new Movimento();
                if (eventoTransfolha.DescricaodoEvento.Equals("RECEBIMENTO DO EDI"))
                {
                    movimento.Evento = "RECEBIDO PELA TRANSPORTADORA";
                }
                else
                {
                    movimento.Evento = eventoTransfolha.DescricaodoEvento;
                }
                movimento.Data = DateTime.ParseExact(eventoTransfolha.DataEvento + " " + eventoTransfolha.HoraEvento, "dd/MM/yyyy HH:mm", CultureInfo.CreateSpecificCulture("pt-BR"));
                eventos.Add(movimento);
            }

            return eventos;
        }

        /// <summary>
        /// Gera a listagem de eventos de rastreio Correios
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <returns></returns>
        public List<Movimento> GerarEventosViaCorreios(PedidoRastreio pedido)
        {
            List<Movimento> eventos = new List<Movimento>();
            CorreiosRastreio rastreioCorreios = RastreioCorreios(pedido);
            foreach (var eventoCorreios in rastreioCorreios.Eventos)
            {
                Movimento movimento = new Movimento();
                movimento.Evento = eventoCorreios.Descricao;
                movimento.Detalhes = eventoCorreios.Detalhe;
                movimento.Data = Convert.ToDateTime(eventoCorreios.Data + " " + eventoCorreios.Hora);
                movimento.Observacao = eventoCorreios.Local + " - " + eventoCorreios.Cidade + " - " + eventoCorreios.Uf;
                eventos.Add(movimento);
            }

            return eventos;
        }

        /// <summary>
        /// Rastreia um objeto na Intelipost
        /// </summary>
        /// <param name="numeroRastreio">Número de rastreio</param>
        /// <returns>Objeto rastreado</returns>
        public IntelipostRastreio RastrearIntelipost(string numeroRastreio)
        {
            RestClient client = new RestClient("https://api.intelipost.com.br/");
            string recurso = $"/api/v1/shipment_order/tracking_code/{numeroRastreio.Trim()}/";

            var requisicao = new RestRequest(recurso, Method.GET)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            requisicao.AddHeader("Accept", "application/json");
            requisicao.AddHeader("api-key", $"8c69cf4d1f33055c1d2b78f9652bf201e74ee7c1dbc4c65c5028d75af9f8b88c");
            requisicao.AddHeader("Content-Type", "application/json");

            IRestResponse resposta = client.Execute(requisicao);

            IntelipostRastreio intelipostRastreio = JsonConvert.DeserializeObject<IntelipostRastreio>(resposta.Content);

            return intelipostRastreio;
        }

        /// <summary>
        /// Rastreia um objeto na Loggi
        /// </summary>
        /// <param name="numeroRastreio">Número de rastreio loggi</param>
        /// <returns>Objeto rastreado</returns>
        public LoggiRastreio RastreioLoggi(string numeroRastreio)
        {
            RestClient client = new RestClient("https://www.loggi.com/api");
            string recurso = $"/v1/pro/package/tracking/{numeroRastreio.Trim()}/";

            var requisicao = new RestRequest(recurso, Method.GET)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            requisicao.AddHeader("Accept", "application/json");
            requisicao.AddHeader("Authorization", $"ApiKey paula.coradi@connectparts.com.br:ef416c3dd955790327ee8d114e4670225ba251b0");

            IRestResponse resposta = client.Execute(requisicao);

            LoggiRastreio loggiRastreio = JsonConvert.DeserializeObject<LoggiRastreio>(resposta.Content);

            return loggiRastreio;
        }

        /// <summary>
        /// Rastreia um objeto na NowLog
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <param name="cnpj">CNPJ de rastreio</param>
        /// <returns>Objeto rastreado</returns>
        public NowLogRastreio RastreioNowLog(PedidoRastreio pedido, string cnpj)
        {
            RestClient client = new RestClient("https://ssw.inf.br/api");
            string recurso = $"/tracking";

            NowLogRequisicao corpo = new NowLogRequisicao()
            {
                cnpj = cnpj,
                senha = "NOW12345",
                nro_nf = pedido.NotaFiscalNumero.ToString(),
            };

            var requisicao = new RestRequest(recurso, Method.POST)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            requisicao.AddHeader("Content-Type", "application/json");
            requisicao.AddJsonBody(corpo);

            IRestResponse resposta = client.Execute(requisicao);

            NowLogRastreio nowLogRastreio = JsonConvert.DeserializeObject<NowLogRastreio>(resposta.Content);

            return nowLogRastreio;
        }

        /// <summary>
        /// Rastreia um objeto na Rodonaves
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <param name="cnpj">CNPJ de rastreio</param>
        /// <returns>Objeto rastreado</returns>
        public RodonavesRastreio RastreioRodonaves(PedidoRastreio pedido, string cnpj)
        {
            RestClient client = new RestClient("https://01wapi.rte.com.br/api/v1/");
            string recurso = $"/rastreio-mercadoria?taxIdRegistration={cnpj}&invoiceNumber={pedido.NotaFiscalNumero}";

            var requisicao = new RestRequest(recurso, Method.GET)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            Token autenticacao = GeraToken(pedido.PedidoTransportadoraCodigo);

            requisicao.AddHeader("Cache-Control", "no-cache");
            requisicao.AddHeader("Authorization", $"Bearer {autenticacao.token}");
            requisicao.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            IRestResponse resposta = client.Execute(requisicao);

            RodonavesRastreio rodoNavesRastreio = JsonConvert.DeserializeObject<RodonavesRastreio>(resposta.Content);

            return rodoNavesRastreio;
        }

        /// <summary>
        /// Rastreia um objeto da Transfolha
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <returns>Objeto rastreado</returns>
        public TransfolhaRastreio RastreioTransfolha(PedidoRastreio pedido)
        {
            Token key = GerarKeyTransfolha();

            Token autenticacao = AutenticarTransfolha(key);

            RestClient client = new RestClient("http://webapi.transfolha.com.br/api/");
            string recurso = "";

            var requisicao = new RestRequest(recurso, Method.POST)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            TransfolhaParametrosRastreio parametros = new TransfolhaParametrosRastreio()
            {
                NotaFiscalCliente = pedido.NotaFiscalNumero.ToString(),
                Serie = pedido.NotaFiscalSerie.ToString(),
                ChaveSessao = autenticacao.token,
            };

            TransfolhaRequisicaoRastreio corpo = new TransfolhaRequisicaoRastreio()
            {
                key = autenticacao.token,
                resource = "0000",
                method = "EDI_Consulta_Pedido",
                parameters = parametros
            };

            requisicao.AddHeader("Content-Type", "application/json");
            requisicao.AddJsonBody(corpo);

            IRestResponse resposta = client.Execute(requisicao);

            TransfolhaRastreio transfolhaRastreio = JsonConvert.DeserializeObject<TransfolhaRastreio>(resposta.Content);

            return transfolhaRastreio;
        }

        /// <summary>
        /// Rastreia um objeto dos Correios
        /// </summary>
        /// <param name="pedido">Objeto de pedido</param>
        /// <returns>Objeto rastreado</returns>
        public CorreiosRastreio RastreioCorreios(PedidoRastreio pedido)
        {
            var serviceClient = new ServiceClient();
            var correiosInterceptor = new CorreiosEndPointBehavior();

            serviceClient.Endpoint.Behaviors.Add(correiosInterceptor);
            serviceClient.buscaEventosLista("0008677036", ">WZWWO9X:C", "L", "T", "101", pedido.NumeroRastreio.Split(','));

            var stringReaderXmlRastro = new StringReader(correiosInterceptor.LastResponseXml);
            var sro = XDocument.Load(stringReaderXmlRastro)
                .Descendants("objeto")
                .FirstOrDefault();

            CorreiosRastreio rastreioCorreios = new CorreiosRastreio(sro);
            return rastreioCorreios;
        }


        /// <summary>
        /// Gera chave de acesso a transfolha 
        /// </summary>
        /// <returns></returns>
        public Token AutenticarTransfolha(Token key)
        {
            RestClient client = new RestClient("http://webapi.transfolha.com.br/api/");
            string recurso = "";

            var requisicao = new RestRequest(recurso, Method.POST)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            TransfolhaParametros parametros = new TransfolhaParametros()
            {
                CodigoCliente = "000818",
                ChaveAcessoCliente = "82BBC100F1",
            };

            TransfolhaRequisicao corpo = new TransfolhaRequisicao()
            {
                key = key.token,
                resource = "0000",
                method = "EDI_LOGIN",
                parameters = parametros
            };

            requisicao.AddHeader("Content-Type", "application/json");
            requisicao.AddJsonBody(corpo);

            IRestResponse resposta = client.Execute(requisicao);

            TransfolhaRetornoChave transfolhaRastreio = JsonConvert.DeserializeObject<TransfolhaRetornoChave>(resposta.Content);

            Token autenticacao = new Token
            {
                token = transfolhaRastreio.Table[0].ChaveSessao
            };

            return autenticacao;
        }

        /// <summary>
        /// Gera key de acesso a transfolha
        /// </summary>
        /// <returns></returns>
        public Token GerarKeyTransfolha()
        {
            RestClient client = new RestClient("http://webapi.transfolha.com.br/api/gerarkey.aspx");
            string recurso = "";

            var requisicao = new RestRequest(recurso, Method.GET)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            IRestResponse resposta = client.Execute(requisicao);

            Token autenticacao = new Token
            {
                token = resposta.Content
            };

            return autenticacao;
        }

        /// <summary>
        /// Gera token de acesso a transportadora baseando-se na transportadora
        /// </summary>
        /// <param name="transportadoraCodigo"></param>
        /// <returns></returns>
        public Token GeraToken(int transportadoraCodigo)
        {
            RestClient client = new RestClient("http://api.dakotaparts.com.br/Frete");
            string recurso = $"/Autenticacao/GerarOuAtualizarToken?transportadoraCodigo={transportadoraCodigo}";

            var requisicao = new RestRequest(recurso, Method.GET)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            string resultadoToken = client.Execute(requisicao).Content;
            Token autenticacao = JsonConvert.DeserializeObject<Token>(resultadoToken);

            return autenticacao;
        }

        /// <summary>
        /// Converte a data de formato Unix para DateTime
        /// </summary>
        /// <param name="dataUnix">Formato unix de data</param>
        /// <returns>Data em datetime</returns>
        public static DateTime ConverterData(double dataUnix)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(dataUnix).ToLocalTime();
            return dtDateTime;
        }


    }
}