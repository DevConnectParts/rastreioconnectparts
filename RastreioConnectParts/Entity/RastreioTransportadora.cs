﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Entity
{
    public class RastreioTransportadora
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool RastreioIntelipost { get; set; }
    }
}