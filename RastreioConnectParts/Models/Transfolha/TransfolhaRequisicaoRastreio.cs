﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Transfolha
{
    public class TransfolhaRequisicaoRastreio
    {
        public string key { get; set; }
        public string resource { get; set; }
        public string method { get; set; }
        public TransfolhaParametrosRastreio parameters { get; set; }

    }

}