﻿namespace RastreioConnectParts.Models.Transfolha
{
    public class TransfolhaParametrosRastreio
    {
        public string NotaFiscalCliente { get; set; }
        public string Serie { get; set; }
        public string ChaveSessao { get; set; }

    }

}