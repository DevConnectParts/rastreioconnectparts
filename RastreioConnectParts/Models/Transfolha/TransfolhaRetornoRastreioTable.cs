﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Transfolha
{
    public class TransfolhaRetornoRastreioTable
    {
        public string DataEvento { get; set; }
        public string HoraEvento { get; set; }
        public string DescricaodoEvento { get; set; }

    }

}