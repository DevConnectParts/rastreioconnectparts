﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Transfolha
{
    public class TransfolhaRequisicao
    {
        public string key { get; set; }
        public string resource { get; set; }
        public string method { get; set; }
        public TransfolhaParametros parameters { get; set; }

    }

}