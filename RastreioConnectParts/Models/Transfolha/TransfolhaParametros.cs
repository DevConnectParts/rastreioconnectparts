﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Transfolha
{
    public class TransfolhaParametros
    {
        public string CodigoCliente { get; set; }
        public string ChaveAcessoCliente { get; set; }

    }

}