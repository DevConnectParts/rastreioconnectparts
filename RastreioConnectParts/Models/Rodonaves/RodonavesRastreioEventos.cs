﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Rodonaves
{
    public class RodonavesRastreioEventos
    {
        public string Date { get; set; }
        public string Description { get; set; }

    }

}