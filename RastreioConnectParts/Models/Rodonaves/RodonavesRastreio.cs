﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Rodonaves
{
    public class RodonavesRastreio
    {
        public RodonavesRastreioEventos[] Events { get; set; }       
    }

}