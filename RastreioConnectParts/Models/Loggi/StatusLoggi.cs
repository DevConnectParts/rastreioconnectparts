﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Loggi
{
    public class StatusLoggi
    {
        public string category { get; set; }
        public string action { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public int updated_at { get; set; }
    }
}