﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.NowLog
{
    public class NowLogRequisicao
    {
        public string cnpj { get; set; }
        public string senha { get; set; }
        public string nro_nf { get; set; }
    }

}