﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.NowLog
{
    public class NowLogRastreioEventos
    {
        public string data_hora { get; set; }
        public string dominio { get; set; }
        public string filial { get; set; }
        public string cidade { get; set; }
        public string ocorrencia { get; set; }
        public string descricao { get; set; }
        public string tipo { get; set; }
        public string data_hora_efetiva { get; set; }
        public string nome_recebedor { get; set; }
        public string nro_doc_recebedor { get; set; }

    }

}