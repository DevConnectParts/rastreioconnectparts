﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models.Intelipost
{
    public class Notify
    {
    }

    public class EndCustomer
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string cellphone { get; set; }
        public bool is_company { get; set; }
        public string federal_tax_payer_id { get; set; }
        public object state_tax_payer_id { get; set; }
        public string shipping_address { get; set; }
        public string shipping_number { get; set; }
        public string shipping_additional { get; set; }
        public string shipping_reference { get; set; }
        public string shipping_quarter { get; set; }
        public string shipping_city { get; set; }
        public string shipping_zip_code { get; set; }
        public string shipping_state { get; set; }
        public string shipping_state_code { get; set; }
        public string shipping_country { get; set; }
        public Notify notify { get; set; }
    }

    public class AdditionalInformation
    {
    }

    public class ExternalOrderNumbers
    {
        public string sales { get; set; }
    }

    public class ShipmentOrderVolumeInvoice
    {
        public string shipment_order_volume_number { get; set; }
        public int shipment_order_volume_id { get; set; }
        public string invoice_series { get; set; }
        public string invoice_number { get; set; }
        public string invoice_key { get; set; }
        public double invoice_total_value { get; set; }
        public double? invoice_products_value { get; set; }
        public string invoice_cfop { get; set; }
        public long invoice_date_iso { get; set; }
        public DateTime invoice_date_iso_iso { get; set; }
    }

    public class ShipmentVolumeMicroState
    {
        public int id { get; set; }
        public string code { get; set; }
        public string default_name { get; set; }
        public string i18n_name { get; set; }
        public string description { get; set; }
        public int shipment_order_volume_state_id { get; set; }
        public int shipment_volume_state_source_id { get; set; }
        public string name { get; set; }
        public string shipment_volume_state { get; set; }
        public string shipment_volume_state_localized { get; set; }
    }

    public class ShipmentOrderVolumeStateHistoryArray
    {
        public int shipment_order_volume_id { get; set; }
        public string shipment_order_volume_state { get; set; }
        public object tracking_state { get; set; }
        public object created { get; set; }
        public DateTime created_iso { get; set; }
        public string provider_message { get; set; }
        public object provider_state { get; set; }
        public object esprinter_message { get; set; }
        public object shipper_provider_state { get; set; }
        public ShipmentVolumeMicroState shipment_volume_micro_state { get; set; }
        public object location { get; set; }
        public object request_hash { get; set; }
        public object request_origin { get; set; }
        public object attachments { get; set; }
        public string shipment_order_volume_state_localized { get; set; }
        public int shipment_order_volume_state_history { get; set; }
        public object event_date { get; set; }
        public DateTime event_date_iso { get; set; }
    }

    public class ShipmentOrderVolumeArray
    {
        public string shipment_order_volume_number { get; set; }
        public int shipment_order_id { get; set; }
        public string shipment_order_volume_state { get; set; }
        public object weight { get; set; }
        public string volume_type_code { get; set; }
        public object width { get; set; }
        public object height { get; set; }
        public object length { get; set; }
        public string products_nature { get; set; }
        public object products_quantity { get; set; }
        public bool is_icms_exempt { get; set; }
        public string logistic_provider_tracking_code { get; set; }
        public ShipmentOrderVolumeInvoice shipment_order_volume_invoice { get; set; }
        public object name { get; set; }
        public long created { get; set; }
        public DateTime created_iso { get; set; }
        public long modified { get; set; }
        public DateTime modified_iso { get; set; }
        public object logistic_provider_label_hash { get; set; }
        public bool has_clarify_delivery_fail { get; set; }
        public bool delivered { get; set; }
        public bool delivered_late { get; set; }
        public bool delivered_late_lp { get; set; }
        public long estimated_delivery_date { get; set; }
        public DateTime estimated_delivery_date_iso { get; set; }
        public string estimated_delivery_date_lp { get; set; }
        public DateTime estimated_delivery_date_lp_iso { get; set; }
        public long original_estimated_delivery_date { get; set; }
        public DateTime original_estimated_delivery_date_iso { get; set; }
        public long? original_estimated_delivery_date_lp { get; set; }
        public DateTime original_estimated_delivery_date_lp_iso { get; set; }
        public object estimated_delivery_days_lp { get; set; }
        public object pre_shipment_list_id { get; set; }
        public object logistic_provider_pre_shipment_list_id { get; set; }
        public object client_pre_shipment_list { get; set; }
        public string pre_shipment_list_state { get; set; }
        public object attachments { get; set; }
        public object products { get; set; }
        public object logistics_provider_data { get; set; }
        public long shipped_date { get; set; }
        public DateTime shipped_date_iso { get; set; }
        public object delivered_date { get; set; }
        public string shipment_order_volume_state_localized { get; set; }
        public int shipment_order_volume_id { get; set; }
        public string tracking_code { get; set; }
        public List<ShipmentOrderVolumeStateHistoryArray> shipment_order_volume_state_history_array { get; set; }
    }

    public class Content
    {
        public int id { get; set; }
        public string platform { get; set; }
        public long created { get; set; }
        public DateTime created_iso { get; set; }
        public int delivery_method_id { get; set; }
        public object delivery_method_external_id { get; set; }
        public string order_number { get; set; }
        public string sales_channel { get; set; }
        public EndCustomer end_customer { get; set; }
        public object scheduled_delivery_date { get; set; }
        public long estimated_delivery_date { get; set; }
        public DateTime estimated_delivery_date_iso { get; set; }
        public object scheduling_window_start { get; set; }
        public object scheduling_window_end { get; set; }
        public bool scheduled { get; set; }
        public string origin_zip_code { get; set; }
        public string origin_federal_tax_payer_id { get; set; }
        public string origin_quarter { get; set; }
        public string origin_city { get; set; }
        public string origin_state_code { get; set; }
        public string origin_street { get; set; }
        public string origin_number { get; set; }
        public object origin_additional { get; set; }
        public string delivery_method_name { get; set; }
        public List<object> markers { get; set; }
        public long modified { get; set; }
        public DateTime modified_iso { get; set; }
        public string sales_order_number { get; set; }
        public object shipment_order_sub_type { get; set; }
        public object return_authorization_code { get; set; }
        public object pudo_id { get; set; }
        public object pudo_external_id { get; set; }
        public long shipped_date { get; set; }
        public DateTime shipped_date_iso { get; set; }
        public string logistic_provider_name { get; set; }
        public double provider_shipping_cost { get; set; }
        public AdditionalInformation additional_information { get; set; }
        public string shipment_order_type { get; set; }
        public object carrier { get; set; }
        public double customer_shipping_costs { get; set; }
        public object estimated_delivery_days_lp { get; set; }
        public object origin_customer_phone { get; set; }
        public object origin_customer_email { get; set; }
        public object origin_reference { get; set; }
        public string origin_name { get; set; }
        public object topic_id { get; set; }
        public ExternalOrderNumbers external_order_numbers { get; set; }
        public List<object> attachments { get; set; }
        public List<ShipmentOrderVolumeArray> shipment_order_volume_array { get; set; }
    }

    public class IntelipostRastreio
    {
        public string status { get; set; }
        public List<object> messages { get; set; }
        public List<Content> content { get; set; }
        public string time { get; set; }
        public string timezone { get; set; }
        public string locale { get; set; }
    }
}