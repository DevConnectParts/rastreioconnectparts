﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace RastreioConnectParts.Models.Correios
{
    public class CorreiosRastreio
    {
        public string NumeroRastreio { get; set; }
        public string Sigla { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public List<EventosCorreios> Eventos { get; set; }

        public CorreiosRastreio(XContainer container)
        {
            NumeroRastreio = container?.Element("numero")?.Value;
            Sigla = container?.Element("sigla")?.Value;
            Nome = container?.Element("nome")?.Value;
            Categoria = container?.Element("categoria")?.Value;
            Eventos = container?.Elements("evento")
                .Select(x => new EventosCorreios(x))
                .ToList();

            Eventos?.RemoveAll(x => x.Tipo.Equals("BLQ"));
        }
    }
}