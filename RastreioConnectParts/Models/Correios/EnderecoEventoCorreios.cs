﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace RastreioConnectParts.Models.Correios
{

    public class EnderecoEventoCorreios
    {
        public string Codigo { get; set; }
        public string Cep { get; set; }
        public string Rua { get; set; }
        public string Complemento { get; set; }
        public string Cidade { get; set; }
        public string Uf { get; set; }
        public string Bairro { get; set; }

        public EnderecoEventoCorreios(XContainer container)
        {
            Codigo = container?.Element("codigo")?.Value;
            Cep = container?.Element("cep")?.Value;
            Rua = container?.Element("logradouro")?.Value;
            Complemento = container?.Element("complemento")?.Value;
            Cidade = container?.Element("localidade")?.Value;
            Uf = container?.Element("uf")?.Value;
            Bairro = container?.Element("bairro")?.Value;
        }
    }

}