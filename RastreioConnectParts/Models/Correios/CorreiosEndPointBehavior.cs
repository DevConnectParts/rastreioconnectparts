﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace RastreioConnectParts.Models.Correios
{
    public class CorreiosEndPointBehavior : IEndpointBehavior
    {
        private readonly CorreiosMessageInspector _correiosMessageInspector = new CorreiosMessageInspector();
        public string LastRequestXml => _correiosMessageInspector.LastRequestXml;
        public string LastResponseXml => _correiosMessageInspector.LastResponseXml;

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(_correiosMessageInspector);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }

}