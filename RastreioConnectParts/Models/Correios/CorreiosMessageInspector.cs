﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.Web;
using System.Web.Services.Description;

namespace RastreioConnectParts.Models.Correios
{
    public class CorreiosMessageInspector : IClientMessageInspector
    {
        public string LastRequestXml { get; set; }
        public string LastResponseXml { get; set; }

        public void AfterReceiveReply(ref System.Web.Services.Description.Message reply, object correlationState)
        {
            LastResponseXml = reply.ToString();
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            LastResponseXml = reply.ToString();
        }

        public object BeforeSendRequest(ref System.Web.Services.Description.Message request, IClientChannel channel)
        {
            LastRequestXml = request.ToString();
            return request;
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            LastRequestXml = request.ToString();
            return request;
        }
    }
}