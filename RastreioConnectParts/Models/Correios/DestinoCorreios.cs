﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace RastreioConnectParts.Models.Correios
{

    public class DestinoEventoCorreios
    {
        public string Local { get; set; }
        public string Codigo { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Uf { get; set; }

        public DestinoEventoCorreios(XContainer container)
        {
            Local = container?.Element("local")?.Value;
            Codigo = container?.Element("codigo")?.Value;
            Cidade = container?.Element("cidade")?.Value;
            Bairro = container?.Element("bairro")?.Value;
            Uf = container?.Element("uf")?.Value;
        }
    }

}