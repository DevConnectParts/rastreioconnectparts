﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace RastreioConnectParts.Models.Correios
{
    public class EventosCorreios
    {
        public string Tipo { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public string Hora { get; set; }
        public string Descricao { get; set; }
        public string Detalhe { get; set; }
        public string Recebedor { get; set; }
        public string Documento { get; set; }
        public string Comentario { get; set; }
        public string Local { get; set; }
        public string Codigo { get; set; }
        public string Cidade { get; set; }
        public string Uf { get; set; }

        public DestinoEventoCorreios Destino { get; set; }

        public EnderecoEventoCorreios Endereco { get; set; }

        public EventosCorreios(XContainer container)
        {
            Tipo = container?.Element("tipo")?.Value;
            Status = container?.Element("status")?.Value;
            Data = container?.Element("data")?.Value;
            Hora = container?.Element("hora")?.Value;
            Descricao = container?.Element("descricao")?.Value;
            Detalhe = container?.Element("detalhe")?.Value;
            Recebedor = container?.Element("recebedor")?.Value;
            Documento = container?.Element("documento")?.Value;
            Comentario = container?.Element("comentario")?.Value;
            Local = container?.Element("local")?.Value;
            Codigo = container?.Element("codigo")?.Value;
            Cidade = container?.Element("cidade")?.Value;
            Uf = container?.Element("uf")?.Value;
            Destino = new DestinoEventoCorreios(container?.Element("destino"));
            Endereco = new EnderecoEventoCorreios(container?.Element("endereco"));
        }
    }

}