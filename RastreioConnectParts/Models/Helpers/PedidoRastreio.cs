﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models
{
    public class PedidoRastreio
    {
        public string NumeroRastreio { get; set; }
        public int NotaFiscalCodigo { get; set; }
        public int NotaFiscalNumero { get; set; }
        public int NotaFiscalSerie { get; set; }
        public int PedidoCodigo { get; set; }
        public string PedidoCodigoExterno { get; set; }
        public int PedidoUnidadeNegocio { get; set; }
        public int PedidoTransportadoraCodigo { get; set; }
        public string PedidoTransportadoraNome { get; set; }
        public int PedidoTransportadoraServicoEntregaCodigo { get; set; }
        public string PedidoTransportadoraServicoEntregaNome { get; set; }
    }
}