﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RastreioConnectParts.Models
{
    public class Movimento
    {
        public DateTime Data { get; set; }
        public string Evento { get; set; }
        public string Observacao { get; set; }
        public string Detalhes { get; set; }

    }

    public class Rastreio
    {
        public string Transportadora { get; set; }
        public string Metodo { get; set; }
        public string Status { get; set; }
        public DateTime DataEstimada { get; set; }
        public List<Movimento> Movimentos { get; set; }
    }

}